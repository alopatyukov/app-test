import { configs } from './src/configs';

module.exports = {
  local: configs.local.db,
  development: configs.development.db,
  production: configs.production.db,
};
