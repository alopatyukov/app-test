import { IConfig } from './interface/config.interface';

export const configDevelopment: IConfig = {
  db: {
    client: 'pg',
    connection: {
      host: 'db',
      user: 'appTest',
      password: 'appTest',
      database: 'app_test',
    },
    migrations: {
      tableName: 'migrations',
      directory: ['./database/migrations'],
    },
  },

};
