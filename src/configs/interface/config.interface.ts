export interface IConfig {
  db: {
    client: string;
    connection: {
      host: string;
      user: string;
      password: string;
      database: string;
    },
    migrations: {
      tableName: string;
      directory?: string[];
    };
    debug?: boolean;
  };
}
