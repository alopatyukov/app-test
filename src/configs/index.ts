import { configLocal } from './local.config';
import { configDevelopment } from './development.config';
import { configProduction } from './production.config';

export const configs = {
  local: configLocal,
  development: configDevelopment,
  production: configProduction,
};
