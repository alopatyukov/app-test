import { IConfig } from './interface/config.interface';

export const configLocal: IConfig = {
  db: {
    client: 'pg',
    connection: {
      host: 'localhost',
      user: 'appTest',
      password: 'appTest',
      database: 'app_test',
    },
    migrations: {
      tableName: 'migrations',
      directory: [ './database/migrations' ],
    },
  },
};
