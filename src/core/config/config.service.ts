import { configs } from '../../configs';

export class ConfigService {
  public readonly envConfig: { [ key: string ]: string };

  constructor(config: string) {
    this.envConfig = configs[ config ];
  }

  get(key: string): any {
    return this.envConfig[ key ];
  }
}
