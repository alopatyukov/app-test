import { Module } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import * as Knex from 'knex';

const providers = [
  {
    provide: 'DbConnection',
    useFactory: (configService: ConfigService) => {
      return Knex(configService.get('db'));
    },
    inject: [ ConfigService ],
  },
];

@Module({
  imports: [ ConfigModule ],
  providers: [ ...providers],
  exports: [ ...providers ],
})
export class DatabaseModule {

}
