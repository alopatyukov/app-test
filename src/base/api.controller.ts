import { ApiService } from './api.service';
import { BadRequestException, Body, ClassSerializerInterceptor, Get, Param, Post, Type, UseInterceptors } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validateSync } from 'class-validator';

export abstract class ApiController<T> {
  protected constructor(protected service: ApiService<T>, protected dto: Type<T>){}

  @UseInterceptors(ClassSerializerInterceptor)
  @Get(':id')
  async getById(@Param('id') id: number): Promise<T> {
    return this.service.getEntityById(id);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Post('')
  async create(@Body() data: T, skipValidation = false): Promise<T> {
    const dataInDto = skipValidation ? data : this.validateAndTransform<T>(data, this.dto);
    return this.service.insertEntity(dataInDto);
  }

  private validateAndTransform<K>(data: any, type: Type<T>, skipMissingProperties = false): K | T {
    const dataInDto = plainToClass(type, data) as unknown as K | T;
    const errors = validateSync(dataInDto, {
      skipMissingProperties,
    });
    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }
    return dataInDto;
  }
}
