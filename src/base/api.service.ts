import * as Knex from 'knex';
import { BadRequestException, Type } from '@nestjs/common';
import { classToPlain, plainToClass } from 'class-transformer';

export class ApiService<T> {
  constructor(protected readonly db: Knex, protected tableName: string, protected dto: Type<T>) {
  }

  async getEntityById(id: number): Promise<T> {
    const data: any[] = await this.db.from(this.tableName).where('id', id).select('*');
    return plainToClass<T, object>(this.dto, data[0]);
  }

  async insertEntity(data: T): Promise<T> {
    const plainData = classToPlain(data);
    try {
      const insertedData = await this.db.table(this.tableName).insert(plainData, '*');
      return plainToClass<T, object>(this.dto, insertedData[0]);
    } catch (e) {
      throw new BadRequestException(e.detail);
    }
  }
}
