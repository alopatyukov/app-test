import { Inject, Injectable } from '@nestjs/common';
import { ApiService } from '../base/api.service';
import { UserModel } from './user.model';
import * as Knex from 'knex';

@Injectable()
export class UserService extends ApiService<UserModel>{
  constructor(@Inject('DbConnection') db: Knex){
    super(db, 'users', UserModel);
  }
}
