import { Controller } from '@nestjs/common';
import { ApiController } from '../base/api.controller';
import { UserService } from './user.service';
import { UserModel } from './user.model';

@Controller('users')
export class UserController extends ApiController<UserModel>{
  constructor(userService: UserService){
    super(userService, UserModel);
  }
}
