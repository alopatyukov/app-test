## Configs
```bash
./configs/

# for local-development
local.config.ts

# for development
development.config.ts

# for production
production.config.ts
```
## Migrations
```bash
# create file of migration 
$ npm run migrate:create-local <migration name>

# run migrations locally
$ npm run migrate:run-local

# rollback migrations locally
$ npm run migrate:rollback-local
```


## Running the local app 
The `development` configuration file is used.
```bash
$ npm run start 
```
## Running the local app with detection  file change
The `local` configuration file is used.
```bash
$ npm run start:dev
```

## Running an application with docker-compose
The `development` configuration file is used.
```bash
$ docker-compose up -d
```

## Running an application with docker-compose (production build)
The `development` configuration file is used.
```bash
$ docker-compose -f docker-compose.prod.yml build backend
$ sudo docker-compose -f docker-compose.prod.yml up  -d

# or

$ sudo ./start-prod.sh
```
## Drop database (docker-compose)
```bash
$ docker-compose down
# or
$ docker-compose -f docker-compose.prod.yml down

$ sudo rm -rf ./database/data
```
